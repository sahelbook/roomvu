//
//  EndPoints.swift
//  RoomVU
//
//  Created by Mohammad Takbiri on 7/20/23.
//

enum EndPoints {
    case getPosts
    
    var rawValue: String {
        switch self {
        case .getPosts:
            return "posts"
        }
    }
}
