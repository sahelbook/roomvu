//
//  EndPointGen.swift
//  RoomVU
//
//  Created by Mohammad Takbiri on 7/20/23.
//

import Foundation

struct EndPointGen {
    let endPoint: EndPoints
    var httpMethod: HTTPMethod = .get
    var body: Data?
    var url: URL {
        return URL(string: BaseURL.base + endPoint.rawValue)!
    }
    
    init(endPoint: EndPoints, httpMethod: HTTPMethod? = .get, body: Data? = nil) {
        self.endPoint = endPoint
        self.httpMethod = httpMethod ?? .get
        self.body = body
    }
}
