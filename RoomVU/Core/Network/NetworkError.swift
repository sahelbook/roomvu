//
//  NetworkError.swift
//  RoomVU
//
//  Created by Mohammad Takbiri on 7/20/23.
//

enum NetworkError: Error {
    case invalidResponse
    case decodingError
}
