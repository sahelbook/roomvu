//
//  HTTPMethod.swift
//  RoomVU
//
//  Created by Mohammad Takbiri on 7/20/23.
//

import Foundation

enum HTTPMethod: String{
    case get = "GET"
}
