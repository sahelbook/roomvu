//
//  Connection.swift
//  RoomVU
//
//  Created by Mohammad Takbiri on 7/20/23.
//

import Foundation

struct Connection {
    
    func load<T: Decodable>(_ endPointGen: EndPointGen, model: T.Type) async throws -> Result<T, NetworkError> {
        
        var request = URLRequest(url: endPointGen.url)
        request.httpMethod = endPointGen.httpMethod.rawValue
        
        //Create URLSession configuration
        let configuration = URLSessionConfiguration.default
        
        //Create the URLSession
        let session = URLSession(configuration: configuration)
        
        //Get the data and response from the API
        guard let (data, response) = try? await session.data(for: request) else {
            return .failure(.invalidResponse)
        }
        
        //Check if it was successful or not.
        guard let responseCode = response as? HTTPURLResponse,
                  responseCode.statusCode == 200
        else {
            return .failure(.invalidResponse)
        }

        //Decode the data in the Model
        guard let result = try? JSONDecoder().decode(T.self, from: data) else {
            return .failure(.decodingError)
        }
        
        //Return the success result
        return .success(result)
    }
    
}
