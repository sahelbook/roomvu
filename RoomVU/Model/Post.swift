//
//  Post.swift
//  RoomVU
//
//  Created by Mohammad Takbiri on 7/20/23.
//

import Foundation

struct Post: Decodable {
    let userId: Int
    let id: Int
    let title: String
    let body: String
}
