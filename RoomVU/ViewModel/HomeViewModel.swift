//
//  ViewModel.swift
//  RoomVU
//
//  Created by Mohammad Takbiri on 7/20/23.
//

import Foundation

class HomeViewModel {
    
    func fetchThePosts() async -> [Post] {
        let endPointGen = EndPointGen(endPoint: .getPosts)
        let result = try? await Connection().load(endPointGen, model: [Post].self)
        
        switch result {
        case .success(let data):
            return data
        case .failure(let error):
            print("There is an error in getting Posts: \(error.localizedDescription)")
        case .none:
            break
        }
        
        return [Post]()
    }
}
