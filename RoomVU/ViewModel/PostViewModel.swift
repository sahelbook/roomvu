//
//  PostViewModel.swift
//  RoomVU
//
//  Created by Mohammad Takbiri on 7/20/23.
//

import Foundation

class PostViewModel {
    
    private let randomNumbers = [-3,-1,-5,-2]
    
    func calculateThePublishDay(postID: Int) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy MMM dd"
        formatter.calendar = Calendar(identifier: .gregorian)
        
        let cal = Calendar.current.date(byAdding: .day, value: (randomNumbers.randomElement() ?? 2) * postID, to: Date())!
        return formatter.string(from: cal)
    }
    
}
