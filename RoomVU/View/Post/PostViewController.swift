//
//  PostViewController.swift
//  RoomVU
//
//  Created by Mohammad Takbiri on 7/20/23.
//

import UIKit

class PostViewController: UIViewController {

    //MARK: - UI
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var danPicture: UIImageView!
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    //MARK: - Variables
    var post: Post
    private var viewModel = PostViewModel()
    
    //MARK: - Initialize
    init(post: Post) {
        self.post = post
        super.init(nibName: "PostViewController", bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configUI()
    }
    
    //MARK: - ConfigUI
    private func configUI() {
        danPicture.layer.cornerRadius = danPicture.frame.size.height/2
        danPicture.layer.shadowOffset = CGSize(width: 1, height: 4)
        
        titleLabel.text = post.title
        dateLabel.text = viewModel.calculateThePublishDay(postID: post.id)
        bodyLabel.text = post.body
    }
}
