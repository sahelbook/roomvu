//
//  HomeViewController.swift
//  RoomVU
//
//  Created by Mohammad Takbiri on 7/20/23.
//

import UIKit
import PKHUD

class HomeViewController: UIViewController {

    //MARK: - UI
    @IBOutlet weak var darkModeSwitch: UISwitch!
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - Variables
    private let viewModel = HomeViewModel()
    private var posts = [Post]()
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configUI()
        getData()
    }

    //MARK: - Config
    private func configUI() {
        if let isDarkMode = UserDefaults.standard.object(forKey: "isDarkMode") as? Bool {
            if isDarkMode {
                navigationController?.overrideUserInterfaceStyle = .dark
                darkModeSwitch.isOn = true
            }else {
                navigationController?.overrideUserInterfaceStyle = .light
                darkModeSwitch.isOn = false
            }
        }
        
        self.title = "Dan's Blog"
        navigationController?.navigationBar.prefersLargeTitles = true

        tableView.register(UINib(nibName: "HomeTableViewCell", bundle: nil),
                           forCellReuseIdentifier: "cell")
        tableView.estimatedRowHeight = 80
    }
    
    private func getData() {
        Task {
            HUD.show(.progress)
            self.posts = await viewModel.fetchThePosts()
            self.tableView.reloadData()
            HUD.hide()
        }
    }
    
    //MARK: - Actions
    @IBAction func darkModeSwitch(_ sender: UISwitch) {
        if sender.isOn {
            navigationController?.overrideUserInterfaceStyle = .dark
            UserDefaults.standard.set(true, forKey: "isDarkMode")
        }else {
            navigationController?.overrideUserInterfaceStyle = .light
            UserDefaults.standard.set(false, forKey: "isDarkMode")
        }
    }
}

//MARK: - UITableViewDelegate, UITableViewDataSource
extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HomeTableViewCell
        cell.configCellForThe(post: posts[indexPath.row])
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = PostViewController(post: posts[indexPath.row])
        navigationController?.pushViewController(vc, animated: true)
    }
}
