//
//  HomeTableViewCell.swift
//  RoomVU
//
//  Created by Mohammad Takbiri on 7/20/23.
//

import UIKit

class HomeTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    override func prepareForReuse() {
        titleLabel.text = nil
        subtitleLabel.text = nil
    }
    
    func configCellForThe(post: Post) {
        self.titleLabel.text = post.title
        self.subtitleLabel.text = post.body
    }
}
